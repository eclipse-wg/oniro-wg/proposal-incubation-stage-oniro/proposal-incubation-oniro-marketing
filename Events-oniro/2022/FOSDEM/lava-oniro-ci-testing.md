# Oniro CI/Testing integration with LAVA

## Track

Testing and Automation

https://lists.fosdem.org/listinfo/testing-automation-devroom

## Title
Oniro CI/Testing integration with LAVA

## Subtitle
Integration of LAVA with gitlab within Oniro CI

## Event type
Lightning Talk (10 min)

## Persons
Stevan Radaković

Stevan worked most of his career on backend in Python, developing
automated systems for testing, among other things. Uses emacs to
prepare coffee. Plays tennis, voleyball and guitar.

## Abstract (one paragraph)
In this session we introduce and explain the integration between LAVA
(Linaro Automated Validation Architecture) and GitLab as part of the
testing efforts in the Oniro OS from Eclipse foundation. The session will cover brief introductions to LAVA and Oniro, integration with GitLab and also present on how we provide vendors the opportunity to test the full software stack on the live devices in-house in completely integrated manner with the main lab and have the test results available for reporting in the upstream gitlab instance.

## Description
In this session we introduce and explain the integration between LAVA
(Linaro Automated Validation Architecture) and GitLab as part of the
testing efforts in the Oniro OS from Eclipse foundation.

The session will cover brief introductions to LAVA and Oniro, integration
with GitLab and also present on how we provide vendors the opportunity to
test the full software stack on the live devices in-house in completely
integrated manner with the main lab and have the test results available
for reporting in the upstream gitlab instance.