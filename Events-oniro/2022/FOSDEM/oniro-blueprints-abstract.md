# Oniro Blueprints

## Track

Embedded, Mobile and Automotive devroom

https://fosdem.org/2022/schedule/track/embedded_mobile_and_automotive/#

## Title

Oniro Blueprints for IoT devices

## Subtitle

From open-source seeds to products.

## Abstract

In the Eclipse Oniro project, a distributed OS for consumer electronics,
we're proposing a collection of "blueprints" use cases and their implementations
using embedded software on reference hardware.

Today, we are going to explain our motivations for the blueprints' approach
and of course how to replicate and deploy firmware from sources and
why Oniro can be flexible to create the device of your dream.

## Description

Oniro is an ambitious Eclipse project,
one of its challenges is to defragment existing IoT ecosystems.

To target a bigger heterogeneous range of IoT devices,
openness, flexibility, and interoperability,
should be enabled at the Operating System level.

Since the beginning of this versatile OS development,
a complementary blueprint concept was introduced
to be more aligned with different aspects of the software and hardware industry.

Blueprints are standalone projects that are addressing specific uses cases
by providing "semi final integration" that would make use of the underneath technology.

Those minimal viable products are not only used for demonstration or validation purposes,
but they can serve as a base to create production-ready solutions.
Blueprints also could inspire devices makers to address similar or more sophisticated use cases.

Today as we speak, our reference blueprints include
a vending machine, an IoT gateway, a door lock, keypad and more.

The results of our work are going to be demonstrated and we will be presenting
what is common and what differs.

Each of those achievements are targeting different uses case and different environments,
but many steps can be factorized from building process, customization to
security or IP compliance scanning.

Expect to see demos on how our Oniro project leverages other opensource projects
like Yocto/OE Embedded distribution, Linux and Zephyr Kernel, LVGL UI toolkit,
OpenThread mesh networking, Web of Things and more.

## Persons

Philippe Coval + potential guest(s) from Oniro team

For years, Philippe Coval is acting as professional OpenSource Engineer,
but he has been involved into software communities since his teenage "Amiga" years.

Over decades, he has contributed to many projects from operating systems
(Debian, MeeGo, Tizen) to IoT frameworks (IoTivity, WebThings) and more.

Creative mind he also shared many proof of concepts in various domains
at various occasions (FOSDEM, ELC, MozFest...),

Currently contributing to the Oniro project,
he is always happy to help and open for new cooperation.

Feel free to reach him at: https://purl.org/rzr/

## Event type

"Lecture" can be flexible on time (15min to 45min)

## Links

Oniro: The Distributed Operating System That Connects Consumer Devices Big and Small
https://oniroproject.org/

Oniro sources
https://booting.oniroproject.org/

Oniro Videos
https://www.youtube.com/watch?v=p-gSvehb-As&list=PLy7t4z5SYNaQBDReZmeHAknEchYmu0LLa#OniroPlaylist

