# Oniro Legal

## Track

Legal & Policy

https://fosdem.org/2022/schedule/track/legal_and_policy_issues_devroom/

Deadline 27/12

## Title

Eclipse Oniro compliance toolchain

## Subtitle

Showcase a compliance toolchain for a Yocto-Based operating system.


## Abstract

Showcasing the toolchain developed for licensing and security compliance of an entire operating system based on Yocto Bitbake. And it's all Free Software!


## Description

Oniro is a Free and Open Source Software, multi-platform, multi-kernel, low spec and embedded interoperable operating system based on Yocto Bitbake. In this context, from the same code base, many different images can be built, with a build matrix counting more than 40 different target images, all containing different sets of software components: which in concrete means millions of source files and -- licensing wise -- a nightmare. Bitbake is incredily flexible, but not quite transparent as to what software is summoned in. Taming this complexity has required a new approach and developing new tools and procedures, also incorporating CVE scanning and security elements, a dedicated dashboard and Aliens4friends, our solution to find licensing information to package matching. Our presentation is a show-and-tell of how we produced a SBOM for as many as 2,000,000 files and almost 300 different licenses with a team of 3.5, also discussing the challenges and some odd discovery about popular packages.


## Persons

Alberto Pianon

A long standing activist and user of Free and Open Source Software, Alberto is a qualified lawyer in private practice in Vicenza, Italy. His practice covers intellectual property, cyberlaw and copyright law, with a particular focus on open source licensing and compliance, especially in the embedded/IoT field. He is member of the Legal Network of the FSFE and partner of OpenChain in Europe together with Carlo Piana (Array). Supporter of FSFE’s Free Your Android initiative and advocate of private cloud and self-hosting solutions. Used to read and write code in Python, PHP, Javascript, shell scripting, Java, C++ as a substantial part of his audit and compliance work on complex projects.


## Event type

30 min talk including 5 min QA


## Links

Oniro: SCA Dashboard
https://sca.ostc-eu.org/

TinfoilHat & Aliens4Friends
https://git.ostc-eu.org/oss-compliance/toolchain/tinfoilhat
https://git.ostc-eu.org/oss-compliance/toolchain/aliens4friends

Oniro: The Distributed Operating System That Connects Consumer Devices Big and Small
https://oniroproject.org/

Oniro sources
https://booting.oniroproject.org/

Oniro Videos
https://www.youtube.com/watch?v=p-gSvehb-As&list=PLy7t4z5SYNaQBDReZmeHAknEchYmu0LLa#OniroPlaylist
