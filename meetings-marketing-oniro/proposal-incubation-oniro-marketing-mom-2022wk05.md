# Oniro WG Incubation stage marketing 2022wk05

* Chair: Agustin
* Scriba: Agustin
* Schedule: 2022-02-02

## Participants

Chiara DF (Huawei), Aurore (Huawei), Gael (EF), Yves (EF), Clark R. (EF), Agustin (EF), Jarek (Huawei), Andrea B. (Synesthesia), Patrick O. (NOI Techpark), Donghi (Huawei) 

## Agenda

* Events list - Agustin 5 min
* FOSDEM - Chiara/Philippe/Agustin 10 min
* MWC - Agustin/Clark 5 min
* Content IP - Agustin 5 min
* AOB - 5 min

## MoM

### Events list

Presentation

Agustin

* The best way in which each organization can help Oniro, specially EF with its ecosystem, leverage over time, is if we plan way in advance the events we would like to go as a group, independently of the events in which each organization attends and would like to promote Oniro.
   * We need to get a common agreement on these events so we support our prospects effort accordingly.
* Because this year COVID had changed most events agendas during 2022Q1, we have had extra time to plan this. I think we should close this topic as soon as possible. 
* When can we have at this forum a proposal for the events prioritised, so we can evaluate it and discuss it? 

Discussion


### FOSDEM

Presentation

Agustin

* Text for the news entry about the Oniro WG SC launch. #task <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/issues/28> 
* Call for participation at the FOSDEM stand.
* Please help in promoting developers to join the libera.chat channel as well as the oniro-dev mailing list.
   * We will route people there.
   * We had a person approaching the IRC channel yesterday asking a technical question that nobody answered. I routed the person to the mailing list. Then I realised that there are almost no engineers there. 

Discussion




### MWC

Presentation

* The ticket to track the invitation creation and distribution is #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/issues/29> 

* There are two different approaches to how to proceed during the event:
   * Put the effort on bringing people to the EF event.
   * Put effort in preparing 1:1 meetings where it is best for our targets.
   
So far, EF has been working assuming the first scenario, Is this assumption not valid any more? Only partially?


Discussion

* Clark summarises the discussion held yesterday at the task force meeting.
   * Scenario 2 should be the priority.
      * Covid restrictions apply. Nobody know us and people is extremely busy. Difficulties to get their attention.
	* Gael agrees. 
* Meetings at venue
   * There is a list of target companies. Call for contacts on that list.
   * 16 out of the current list are sponsoring so they have booth.
   * We need to tailor the message to the profile of the person we meet.
* What will we do as follow up of the event?
   * Agreement on defining the follow up now.
   * Gael suggest to have an Oniro WG SC open call (or similar).
* Chiara will manage the cancellation of the meeting rooms.
* #task we need a collateral to hand over to the people we meet.
   * We can drop the flyers
   * We will need to print the one pager and get a cover for it.


### Content IP

Presentation

* Agustin sent a proposal to be able to use the content from the agency for FOSDEM.
   * Will Huawei be on time to sign the document and send it to us so we can process it and give the ok?
      * Chiara confirms the reception of the design transfer agreement form.

Discussion



### AOB

* The general discussion mailing list for marketing topics has been requested.
* Reminder: every Oniro group meeting or 1:1 meetings where topics are discussed that affect the project or the WG should have minutes and sent to the official channels
   * oniro-wg ML for the Working Group
   * oniro-dev ML for the Oniro projects.
* We will organise a session where EF staff will provide information about what is the Marketing plan, why is so relevant, what minimum content should be on it, how it relates to the Program Plan and the budget and how other WG are doing it. The Q&A is expected. Clark R. from EF, will lead this session.



## Relevant links

* Repo: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/tree/main/>
* Actions workflow board: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/boards/880>
* Actions priority board: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/boards/892> 
* Other meetings: #link <https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/-/wikis/Meetings> 
