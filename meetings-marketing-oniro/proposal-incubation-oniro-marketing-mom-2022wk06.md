# Oniro WG Incubation stage marketing 2022wk06

* Chair: Agustin
* Scriba: Agustin
* Schedule: 2022-02-09

## Participants

Aurore (Huawei), Gael (EF), Yves (EF), Clark R. (EF), Agustin (EF), Sebasstian (Huawei), Donghi (Huawei), Philippe C. (Huawei) 

## Agenda

* MWC - Chiara 10 min
* FOSDEM recap - Philippe 10 min
* AOB - 10 min

## MoM

### MWC 2022 (we moved this topic to Friday)

Presentation

Pending tasks:
* #task we need a collateral to hand over to the people we meet.

Links
* The ticket to track the invitation creation and distribution is #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/issues/29>
* Wiki page #link <https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/-/wikis/Events/MWC>

Discussion


### FOSDEM recap

Presentation

* Philippe presented some numbers in a slide.
* #link to the summary shown by Philippe <https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/-/wikis/Events/FOSDEM#impact>
* A report is being created by the participantss from Oniro.

Discussion

* Positive feedback. Good attendance in our stand.
* Philippe describes and interesting format for online conferences. Content is publishes ahead of the event and the sessions deal with discussions about the topic.
* Conversation about publishing something about our participation at FOSDEM:
   * Blog post
   * Community news.

### AOB

* Oniro communication policy and CoC sent to mailing lists. Please review.
* In two Wednesday we will use this meeting to describe the Marketing Plan as well as its relation with the Program Plan and the budget.
* Reminders
   * Nomination period for the Marketing Committee members among Silver Member organizations ends this Friday.
      * The announcement was sent to the oniro-wg mailing list. #link <https://www.eclipse.org/lists/oniro-wg/msg00135.html>  
   * Every Oniro group meeting or 1:1 meetings where topics are discussed that affect the project or the WG should have minutes and sent to the official channels
      * Oniro-wg ML for the Working Group
      * oniro-dev ML for the Oniro projects.

## Relevant links

* Repo: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/tree/main/>
* Actions workflow board: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/boards/880>
* Actions priority board: #link <https://gitlab.eclipse.org/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/proposal-incubation-oniro-marketing/-/boards/892> 
* Other meetings: #link <https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/proposal-incubation-stage-oniro/-/wikis/Meetings> 
